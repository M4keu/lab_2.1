package com.company;

public class Planet {
    public double x;
    public double y;
    public double z;

    public Planet (double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static double calculateDistance(Planet planet_1, Planet planet_2) {
        return Math.sqrt(Math.pow(planet_1.x - planet_2.x, 2) + Math.pow(planet_1.y - planet_2.y, 2) + Math.pow(planet_1.z - planet_2.z,2));
    }
}
