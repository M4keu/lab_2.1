package com.company;

public class Main {

    public static void main(String[] args) {

        Planet earth = new Planet(20.3, 100.3, 109);
        Planet sun = new Planet(0, 0, 0);

        System.out.println(Planet.calculateDistance(earth, sun));
    }
}
